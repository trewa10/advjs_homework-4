/*
1. Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.
    AJAX це термін що позначає мережеві запити на сервер та отримання відповідей. Запити дозволяють отримувати нову інформацію від сервера в реальному часі,
    відправляти інформацію на сервер, вносити зміни чи видаляти. Це необхідно для відображення постів у блозі, підвантаження списку товарів із сервера тощо. 
    В сучасному JS існує кілька способів отримати інформацію із сервера, старіший XMLHttpRequest, сторонні бібліотеки як Axios та метод fetch(), який повертає проміс
*/

"use strict";

const url = 'https://ajax.test-danit.com/api/swapi/films';
const root = document.querySelector('.movies-info');
// знайшов такі альтернативні постери))
const posters = ['https://alternativemovieposters.com/wp-content/uploads/2021/07/DevinSchoeffler_PhantomMenace.jpg',
                 'https://alternativemovieposters.com/wp-content/uploads/2021/07/DevinSchoeffler_AttackOfTheClones.jpg',
                 'https://alternativemovieposters.com/wp-content/uploads/2021/07/DevinSchoeffler_RevengeOfTheSith.jpg',
                 'https://alternativemovieposters.com/wp-content/uploads/2020/11/DevinSchoeffler_NewHope.jpg',
                 'https://alternativemovieposters.com/wp-content/uploads/2020/11/DevinSchoeffler_jedi.jpg',
                 'https://alternativemovieposters.com/wp-content/uploads/2020/11/DevinSchoeffler_Empire.jpg']

// оскільки необхідно два рівні запитів, то додав просту функцію 
const getInfo = (url) => {
  return fetch(url)
    .then(response => response.json())
}

const showSWInfo = function(url, dest) {
  getInfo(url)
    .then(data => {
      data.forEach(({episodeId, name, openingCrawl, characters}) => {
        dest.insertAdjacentHTML('beforeend', `
					<div class="movie-item">
            <div class="movie-wrapper">
              <div class="movie-poster">
                <img src="${posters[episodeId - 1]}" alt="poster star wars episode ${episodeId}">
              </div>
              <div class="movie-content">
                <p class="movie-title">Episode ${episodeId}</p>
                <p class="movie-name">${name}</p>
                <div class="characters movie-chars-${episodeId}"><span>Characters(${characters.length}):</span>
                  <div class="loader"></div>
                </div>
              </div>
            </div>
            <p class="movie-desc">${openingCrawl}</p>
					</div>
				`);
        
        // місце куди будуть додаватись персонажі
        let charsDest = document.querySelector(`.movie-chars-${episodeId}`);

        // враховуючи, що кожен персонаж це окремий запит, то вирішив випробувати Promise.all
        // він приймає масив, а characters якраз масив API, тому просто огорнув отримані посилання в fetch
        let promises = characters.map(charUrl => getInfo(charUrl));

        Promise.all(promises)
          .then((values) => {
            // імена персонажів зібрав в масив і зробив з нього строку виключно для краси (щоб після імені останнього персонажа не відображалась кома)
            let charsCollection = values.map(({name}) => name).join(', ')
            charsDest.insertAdjacentHTML('beforeend', charsCollection)
          })
          .catch(er => {
            charsDest.insertAdjacentHTML('beforeend', 'Список персонажів тимчасово недоступний');
            console.log(er.message);
          })
          .finally(() => charsDest.querySelector('.loader').remove())
      })
    })
    .catch(err => {
      document.body.insertAdjacentHTML('beforeend', 'Список фільмів тимчасово недоступний ;(');
      console.log(err.message)
    })
}

showSWInfo(url, root)



